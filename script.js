let currentTempInC = 0
let currentTempInF = null

let getCurrentTemp = function () {
    return 23
}

function cToF (cVal) {
    let fVal = cVal * (9/5) + 32
    return fVal
}

// currentTempInC = getCurrentTemp()
// currentTempInF = cToF(currentTempInC)
// console.log(currentTempInF + " degrees F")

// 0, 10, 20, 30, 40
// 90F is too hot
// less than 60 is too cold

// for (let temp=0; temp <= 40; temp += 5) {
//     const cTemp = cToF(temp)
//     if (cTemp<60) {
//         console.log(temp + "C is too cold (" + cTemp + "F)")
//     } else if (cTemp>=90) {
//         console.log(temp + "C is too hot (" + cTemp + "F)")
//     } else {
//         console.log(temp + "C is comfortable (" + cTemp + "F)")
//     }
// }

let temp=0
while (temp <= 40) {
    const cTemp = cToF(temp)
    if (cTemp<60) {
        console.log(temp + "C is too cold (" + cTemp + "F)")
    } else if (cTemp>=90) {
        console.log(temp + "C is too hot (" + cTemp + "F)")
    } else {
        console.log(temp + "C is comfortable (" + cTemp + "F)")
    }
    temp += 5
}



// while (getCurrentTemp() > 0) {
//     displaySun()
// }
// displaySnowflake()